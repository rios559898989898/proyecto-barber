<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;





use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;

use App\Perfil;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return "texto desde perfil";
        //DB::table('perfil')->insert(
    //['id' => 3, 'nombre' => "Ricardo", 'direccion' => "Cholula", 'telefono' => "8754321548", 'credito' => 5.2, 'status' => 1]);
        $perfiles = DB::table('perfils')->get();
        $user = DB::table('perfils')->where('nombre', 'juan')->first();
        $nombre=$user->nombre;
        //echo $nombre;
        //dd($perfiles);
        return view('perfil_ver',compact('perfiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
          return view ('insert');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $perfil= new Perfil;

        $perfil->nombre=$request->input('nombre');
        $perfil->direccion=$request->input('direccion');
        $perfil->telefono=$request->input('telefono');
        
        $perfil->save();
        return 'Datos insertados con exito';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $unperfil =  Perfil::find($id);
        return view ('editar',['unperfil'=> $unperfil]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $unperfil =  Perfil::find($id);
        $unperfil->nombre=$request->nombre;
        $unperfil->direccion=$request->direccion;
        $unperfil->telefono=$request->telefono;
        $unperfil->credito=$request->credito;
        $unperfil->status=$request->status;
         $unperfil->save();
         return ('datos actualizados con esxito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Perfil:: destroy($id);
        return ('Datos eliminados con exiro');
    }
}
