<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BarmanController extends Controller
{
    public function index()
    {
    	return "texto desde controller BarmanController";
    }

    public function contacto()
    {
    	return view('contacto');
    }
}
