<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Perfil extends Model
{
    //
    protected $fillable = ['nombre', 'direccion','telefono','credito','status'];
}
