<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route:: get("/clientes", function(){
				$nombre="Ricardo Rios. ";
				$suma=2+6;
			return "Bienvenido cliente: " .$nombre."La suma es: ".$suma;

});


Route:: get("/empleados", function(){
				$nombre="Ricardo Rios. ";
				$suma=2+6;
			return "Bienvenido cliente: " .$nombre."La suma es: ".$suma;

});




//Route:: get("barman","BarmanControlle@index");
//Route:: get("contacto","BarmanControlle@contacto");
Route::resource('cliente', "ClienteController");
Route::resource('productos', "ProductosController");
Route::resource('preparacion', "PreparacionesController");
Route::resource('servicio', "ServicioController");

Route::resource('usuario', 'UserController');
Route::resource('perfil', 'PerfilController');
Route::resource('publica', 'PublicacionController');
Route::resource('comenta', 'ComentarioController');

//Route::get('create', "ReparacionesController@create");


Route::get('edit&{id}', "PerfilController@edit");



Route::get('eliminar&{id}', "PerfilController@destroy");

Route::post('update&{id}', "PerfilController@update");

